const dotEnv = require('dotenv');
const mongoose = require('mongoose');

const logger = require('./utils/logger');

process.on('uncaughtException', err => {
  logger.error('❌ UNCAUGHT EXCEPTION ❌');
  logger.error('Shutting down...');
  logger.error(err);
  setTimeout(() => process.exit(1), 3000);
});

dotEnv.config({ path: './config.env' });

const app = require('./app');

const port = process.env.PORT || 8000;
const host = '127.0.0.1';
const server = app.listen(port, () => {
  logger.info(`Server is running on http://${host}:${port} ...`);
  logger.info(`application is in ${process.env.NODE_ENV} mode`);
});
server.setTimeout(30 * 60 * 1000);

require('./db');

process.on('unhandledRejection', err => {
  if (
    (err.message &&
      ((err.code === 'ESERVFAIL' && err.hostname.includes('mongodb')) ||
        err.message.includes('querySrv ESERVFAIL _mongodb._tcp'))) ||
    err.message.includes('MongoDB Atlas')
  ) {
    // do nothing
  } else {
    logger.error('❌ UNHANDLED REJECTION ❌');
    logger.error('Shutting down...');
    logger.error(err);
    console.log(err);
    setTimeout(
      () =>
        server.close(() => {
          process.exit(1);
        }),
      3000
    );
  }
});

process.on('SIGTERM', () => {
  logger.error('✋ SIGTERM RECEIVED.');
  logger.error('👋👋👋👋👋Shutting down gracefully...');
  setTimeout(
    () =>
      server.close(() => {
        logger.error('❌ Process terminate ❌');
      }),
    1000
  );
});
